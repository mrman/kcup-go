# `kcup`

`kcup` is a single file server. It responds to every `GET` request it receives with the content of a given file (specified by ENV or CLI argument), and for every other request (with any other HTTP method or path) it returns a 404.

`kcup` was invented to help with cases where a generally small file needs to be delivered at a certain path, for example [MTA STS's `/.well-known/mta-sts.txt`](https://en.wikipedia.org/wiki/MTA-STS). You can read more about this [on the associated blog post](https://vadosware.io/post/serving-a-single-file-over-http-with-rust-and-go/).

**BEWARE** This project and repo is so simple (right now) that there aren't even any tests -- I may add some in the future after light use. The machinery (Makefile targets, CI integration, etc) is there for unit, integration and e2e tests, but there just are none.

See also [`kcup-rust`](https://gitlab.com/mrman/kcup-rust)

# Quickstart

`kcup` only needs the path to a single file to run:

```console
$ kcup <file path>
```

By default, `kcup` will serve the file at host `127.0.0.1` on port `5000`. `kcup` can also take file content from STDIN like so:

```console
$ kcup <<EOF
> your file content
> goes here
> EOF
```

## Docker

To run `kcup` with docker:

```console
$ docker run --detach \
-p 5000:5000 \
-e HOST=0.0.0.0 \
-e FILE=/data/file \
-v /path/to/folder/with/your/file:/data \
--name kcup-go \
registry.gitlab.com/mrman/kcup-go/cli:v2
```

Note that `v1` of `kcup-go` used `net/http`, and `v2` uses [`fasthttp`](https://github.com/valyala/fasthttp).

# Usage

```console
$ ./kcup --help
Usage of ./kcup:
  -file string
        File to read
  -host string
        Host
  -port int
        Port (default -1)
  -stdin-read-timeout-seconds int
        Amount of seconds to wait for input on STDIN to serve (default -1)
```

# Environment Variables

| ENV variable                 | Default     | Example              | Description                                      |
|------------------------------|-------------|----------------------|--------------------------------------------------|
| `HOST`                       | `127.0.0.1` | `0.0.0.0`            | The host on which `kcup` will listen             |
| `PORT`                       | `5000`      | `3000`               | The port on whcih `kcup` will listen             |
| `FILE`                       | N/A         | `/path/to/your/file` | The path to the file that will be served         |
| `STDIN_READ_TIMEOUT_SECONDS` | `60`        | `10`                 | The amount of seconds to try and read from STDIN |

# FAQ

## Why is this project named "kcup"?

[Keurig created a "K-Cup" brewing system](https://en.wikipedia.org/wiki/Keurig#Keurig_K-Cup_brewing_systems) that has become somewhat infamous.
